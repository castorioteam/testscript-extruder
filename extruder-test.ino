#define E_STEP_PIN         26
#define E_DIR_PIN          28
#define E_ENABLE_PIN       24

#define STEPS_PER_MM       145.54055  
#define DISTANCE           100 //mm
#define SPEED              50 // mm/min

#define LED_PIN              13 //LED for visualisation



bool run = false;
void setup() {
  pinMode(E_STEP_PIN  , OUTPUT);
  pinMode(E_DIR_PIN    , OUTPUT);
  pinMode(E_ENABLE_PIN    , OUTPUT);
  //pinMode(LED_PIN    , OUTPUT);
  digitalWrite(E_ENABLE_PIN    , LOW);
  /*Richtung
  High oder LOW um Richtuing zu ändern
  */
  digitalWrite(E_DIR_PIN    , LOW);
  Serial.begin(9600);
  Serial.println("Ready to go type 'start' to start");
 
}

void loop() {
  if(Serial.available() > 0)
    {
        String str = Serial.readStringUntil('\n');
        if(str == "start"){
          run = true;
        }
    }
  if(run == true){
    float mathSteps = STEPS_PER_MM * DISTANCE;
    int stepsToGo = (int) mathSteps;
    int stepsMade = 0;
    float waitTime = (((60/STEPS_PER_MM)*1000)/SPEED)-1 ;
    
    if(waitTime < 1){
      waitTime = 1;
    }
    float timeToGo = (stepsToGo * (waitTime+1))/1000;
    Serial.println("Starting to extrude");
    Serial.print("Extruding ");
    Serial.print(DISTANCE);
    Serial.print("mm (");
    Serial.print(stepsToGo);
    Serial.print(" Steps(");
    Serial.print(waitTime);
    Serial.print(")) in ");
    Serial.print(timeToGo);
    Serial.println(" Seconds");
     while (stepsMade < stepsToGo){
        digitalWrite(E_STEP_PIN    , HIGH);
        //digitalWrite(LED_PIN    , HIGH);
        delay(1);
        digitalWrite(E_STEP_PIN    , LOW);
        //digitalWrite(LED_PIN    , LOW);
        stepsMade++;
        delay(waitTime);
        
     }
     Serial.println("Extrusion Done");
     run = false;
  }
   

}


